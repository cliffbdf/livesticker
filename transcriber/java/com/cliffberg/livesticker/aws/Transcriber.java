package com.cliffberg.livesticker.aws;

import java.io.StringReader;
import java.net.URI;
import java.util.Date;
import java.io.File;
import java.io.IOException;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.json.JsonReader;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonArray;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.CommonProperties;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.transcribe.AmazonTranscribeClient;
import com.amazonaws.services.transcribe.AmazonTranscribeClientBuilder;
import com.amazonaws.services.transcribe.AmazonTranscribe;
import com.amazonaws.services.transcribe.model.StartTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.StartTranscriptionJobResult;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobResult;
import com.amazonaws.services.transcribe.model.TranscriptionJobStatus;
import com.amazonaws.services.transcribe.model.TranscriptionJob;
import com.amazonaws.services.transcribe.model.Transcript;
import com.amazonaws.services.transcribe.model.GetTranscriptionJobRequest;
import com.amazonaws.services.transcribe.model.LanguageCode;
import com.amazonaws.services.transcribe.model.MediaFormat;
import com.amazonaws.services.transcribe.model.Media;

public class Transcriber {
	
	/**
	 * Transcribe the media file at the specified URL.
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.out.println(usage());
			System.exit(1);
		}
		
		System.out.println((new Transcriber()).transcribe(new URI(args[0]),
			args[1], args[2]));
	}
	
	public static String usage() {
		return "args:\n" +
			"\turl - wav file to transcribe\n" +
			"\taws_access_key_id\n" +
			"\taws_secret_access_key";
	}
	
	public URI pushFileToS3(File file, String bucketName, String aws_access_key_id,
		String aws_secret_access_key) throws Exception {
	
		String clientRegion = "us-east-1";
		String fileObjKeyName = file.getName();

		try {
			AWSCredentials credentials = new BasicAWSCredentials(
				aws_access_key_id, aws_secret_access_key);
			AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
					.withRegion(clientRegion)
					.withCredentials(new AWSStaticCredentialsProvider(credentials))
					.build();
		
			// Upload a file as a new object with ContentType and title specified.
			PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, file);
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentType("plain/text");
			metadata.addUserMetadata("x-amz-meta-title", "someTitle");
			request.setMetadata(metadata);
			s3Client.putObject(request);
		}
		catch(AmazonServiceException e) {
			// The call was transmitted successfully, but Amazon S3 couldn't process 
			// it, so it returned an error response.
			e.printStackTrace();
		}
		catch(SdkClientException e) {
			// Amazon S3 couldn't be contacted for a response, or the client
			// couldn't parse the response from Amazon S3.
			e.printStackTrace();
		}
		
		URI objectURI = new URI("http://s3.amazonaws.com/" + bucketName + "/" + file.getName());
		return objectURI;
	}
	
	/**
	 * Invoke AWS Transcribe to transcribe the audio file at the specified URI.
	 * API ref: https://docs.aws.amazon.com/transcribe/latest/dg/API_StartTranscriptionJob.html
	 */
	public String transcribe(URI mediaFileUri, String aws_access_key_id,
		String aws_secret_access_key) throws Exception {
		
		AmazonTranscribeClientBuilder builder = AmazonTranscribeClient.builder();
		AmazonTranscribe transcribe = builder.defaultClient();
		AWSCredentials credentials = new BasicAWSCredentials(
			aws_access_key_id, aws_secret_access_key);
		String jobName = createUniqueJobName();
		StartTranscriptionJobRequest request = (new StartTranscriptionJobRequest())
			.withMedia(new Media().withMediaFileUri(mediaFileUri.toString()))
			.withMediaFormat(MediaFormat.Wav)
			.withMediaSampleRateHertz(16000)
			.withTranscriptionJobName(jobName)
			.withLanguageCode(LanguageCode.EnUS)
			.withRequestCredentialsProvider(new AWSStaticCredentialsProvider(credentials))
			//.withSdkClientExecutionTimeout(5000)  // 5 seconds
			//.withSdkRequestTimeout(....)
			//.withSettings()
			;
		StartTranscriptionJobResult res = transcribe.startTranscriptionJob(request);
		
		int seconds = 0;
		GetTranscriptionJobRequest r = (new GetTranscriptionJobRequest())
			.withTranscriptionJobName(jobName)
			.withRequestCredentialsProvider(new AWSStaticCredentialsProvider(credentials))
			;
			
		GetTranscriptionJobResult result = transcribe.getTranscriptionJob(r);
		TranscriptionJob job = result.getTranscriptionJob();
		String status = job.getTranscriptionJobStatus();
		
		while (status.equals("IN_PROGRESS")) {
			if (seconds > 300) throw new Exception("Exceeded time limit");
			result = transcribe.getTranscriptionJob(r);  // <- you have to get the result again
			job = result.getTranscriptionJob();
			status = job.getTranscriptionJobStatus();
			System.out.print(".");
			try {
				Thread.sleep(1000);
				seconds++;
			}
			catch (InterruptedException e) {
				throw new Exception("Thread.sleep() was interrupted");
			}
		}
		System.out.println();
		System.out.println("Transcription job done");
		
		if (! TranscriptionJobStatus.fromValue(status).equals(TranscriptionJobStatus.COMPLETED)) {
			throw new Exception("Transcription status is not COMPLETED: it is " + status);
		}
		Transcript transcription = job.getTranscript();
		String transcriptFileUri = transcription.getTranscriptFileUri();
		Response response = makeGetRequest(transcriptFileUri);
		if (response == null) throw new Exception("Response is null");
		if (response.getStatus() >= 300) {
			throw new Exception(response.getStatusInfo().getReasonPhrase());
		}
		
		// Transcribed text is the response body.
		String body = null;
		try {
			body = response.readEntity(String.class);
		} catch (Exception ex) {
			throw new Exception("Unable to read response body", ex);
		}
		
		// Parse response.
		JsonReader reader = Json.createReader(new StringReader(body));
		JsonObject json = reader.readObject();
		JsonObject results = json.getJsonObject("results");
		JsonArray transcripts = results.getJsonArray("transcripts");
		JsonObject transcript = transcripts.getJsonObject(0);
		String transcribedText = transcript.getString("transcript");
		reader.close();
		
		return transcribedText;
	}
	
	protected Response makeGetRequest(String path, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(path, params);
		Response response = builder.buildGet().invoke();
		return response;
	}
	
	protected Invocation.Builder createHttpRequestBuilder(String path, String[]... params)
		throws Exception {
		
		URI uri = new URI(path);
		
		Registry<ConnectionSocketFactory> socketFactoryRegistry =
			RegistryBuilder.<ConnectionSocketFactory>create()
				.register("https", new SSLConnectionSocketFactory(SSLContext.getDefault()))
				.build();
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.connectorProvider(new ApacheConnectorProvider());
		clientConfig.property(CommonProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);

		BasicHttpClientConnectionManager connManager =
			new BasicHttpClientConnectionManager(socketFactoryRegistry);
		//clientConfig.register(ResponseStatusExceptionFilter.class);
		//clientConfig.register(JsonClientFilter.class);
		//clientConfig.register(JacksonJsonProvider.class);
		clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, connManager);
	
		ClientBuilder clientBuilder = ClientBuilder.newBuilder().withConfig(clientConfig);
		
		Client client = clientBuilder.build();
		
		WebTarget target = client.target(path);
		
		for (String[] keyValuePair : params) {
			if (keyValuePair.length != 2) throw new RuntimeException(
				"Expected a key, value pair; found: " + keyValuePair.toString());
			target = target.queryParam(keyValuePair[0], keyValuePair[1]);
		}
		
		Invocation.Builder invocationBuilder =
			target.request(MediaType.TEXT_PLAIN_TYPE);
		
		return invocationBuilder;
	}

	/**
	 * Must satisfy ^[0-9a-zA-Z._-]+
	 */
	protected String createUniqueJobName() {
		return String.valueOf((new Date()).getTime());
	}
}
