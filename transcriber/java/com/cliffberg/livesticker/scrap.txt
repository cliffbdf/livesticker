REST code that I can't use at present because I don't know the endpoint URL
of the Google Transcribe service, so I am using their Java API instead (which
obscures the endpoint URL).


		int BUFFER_SIZE = 4096;
		String filePath = file.getAbsolutePath();
		SimpleDateFormat df = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);
		Date date = new Date();
		String formattedDate = df.format(date);
		if (!(file.isFile() && file.exists())) {
			throw new Exception("File " + file.toString() + " not found");
		}
		
		URL url = new URL("http://" + bucketName + ".s3.amazonaws.com/" + file.getName());
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		
		// Ref: https://docs.aws.amazon.com/AmazonS3/latest/dev/RESTAuthentication.html#ConstructingTheAuthenticationHeader
		/*
		StringToSign = HTTP-Verb + "\n" +
			Content-MD5 + "\n" +     (optional)
			Content-Type + "\n" +
			Date + "\n" +
			CanonicalizedAmzHeaders +   (only when using x-amz- headers)
			CanonicalizedResource;

		*/
		String httpVerb = "PUT";
		String contentType = "application/octet-stream";
		String resource = "/" + bucketName + "/" + file.getName();
		
		String stringToSign =
			httpVerb + "\n\n" + contentType + "\n" + formattedDate + "\n" + resource;
		

		/*
		Signature = Base64
		(
			HMAC-SHA1
			(
				YourSecretAccessKeyID,
				UTF-8-Encoding-Of( StringToSign )
			)
		);
		*/
		
		Mac hmac = Mac.getInstance("HmacSHA1");
		hmac.init(new SecretKeySpec(aws_secret_access_key.getBytes("UTF-8"), "HmacSHA1"));
		String signature = Base64.getEncoder()
			.encodeToString(
				hmac.doFinal(
					stringToSign.getBytes("UTF-8")))
			.replaceAll("\n", "")
			;
		 
		String authAWS = "AWS " + aws_access_key_id + ":" + signature;
		httpConn.setDoOutput(true);
		httpConn.setRequestMethod(httpVerb);
		httpConn.setRequestProperty("Accept", "*/*");
		httpConn.setRequestProperty("Date", formattedDate);
		httpConn.setRequestProperty("Content-type", contentType);
		httpConn.setRequestProperty("Authorization",authAWS);
		OutputStream outputStream = httpConn.getOutputStream();
		FileInputStream inputStream = new FileInputStream(file);
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			outputStream.write(buffer, 0, bytesRead);
		}
		outputStream.close();
		inputStream.close();
		
		int responseCode = httpConn.getResponseCode();
		if (responseCode >= 300) {
			System.err.println("Put failed with code " + responseCode + "; " + httpConn.getResponseMessage());
			System.err.println("url=" + url);
			System.err.println("authAWS=" + authAWS);
			System.err.println("Content-type=" + contentType);
			System.err.println("Date=" + formattedDate);
			throw new Exception(
				"Put failed with code " + responseCode + "; " + httpConn.getResponseMessage());
		}

		
		
		
		
		// Compose REST request.
		
		JsonObjectBuilder media = Json.createObjectBuilder()
			.add("MediaFileUri", mediaFileUri.toString());
	
		JsonObject model = Json.createObjectBuilder()
			.add("LanguageCode", "en-US")
			.add("Media", media)
			
			.add("MediaFormat", "wav")
			.add("MediaSampleRateHertz", 16000)
			
			.add("TranscriptionJobName", createUniqueJobName())
			.build();
		
		StringWriter stWriter = new StringWriter();
		try (JsonWriter jsonWriter = Json.createWriter(stWriter)) {
			jsonWriter.writeObject(model);
		}
		String jsonPayload = stWriter.toString();

		// Send REST request.
		Response response = null;
		try {
			response = makePostRequest("/StartTranscriptionJob",
				MediaType.APPLICATION_JSON_TYPE, jsonPayload);
			if (response == null) throw new Exception(
				"POST request to AWS Transcribe did not return a response");
			
			// Verify success and obtain container Id.
			if (response.getStatus() >= 300) throw new Exception(
				response.getStatusInfo().getReasonPhrase());
		} finally {
			if (response != null) response.close();
		}
		
		// Parse response and obtain the URI of the transcribed text.
		// Transcription is asynchronous, so the URI might take awhile to
		// become available.
		String responseBody = response.readEntity(String.class);
		JsonReader reader = Json.createReader(new StringReader(responseBody));
		JsonStructure json = reader.read();
		String transcriptFileUri = ((JsonObject)json).getString("Transcript");
		
		// Keep trying to get the URI.
		String transcribedText;
		for (int i = 0; i < 20; i++) {
			response = makeGetRequest(transcriptFileUri);
			if (response == null) throw new Exception("Response is null");
			if (response.getStatus() < 300) {
				// Transcribed text is the response body.
				try {
					transcribedText = response.readEntity(String.class);
				} catch (Exception ex) {
					throw new Exception("Unable to read transcribed text", ex);
				}
				return transcribedText;
			}
			
			Thread.sleep(200);
		}
		
		throw new Exception(
			"Could not obtain transcript; response status: " + response.getStatus());
	}
	
	protected Response makeGetRequest(String path, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(path, params);
		Response response = builder.buildGet().invoke();
		return response;
	}
	
	/**
	 * Perform a POST request. Query parameters are provided as a Map, or may
	 * be null. body may be null.
	 * IMPORTANT: It is essential to call close() on the Response object.
	 */
	protected Response makePostRequest(String path, MediaType contentType,
		String body, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(path, params);
		Response response = null;
		if (body == null) {
			response = builder.buildPost(null).invoke();
		} else {
			Entity<String> entity = Entity.entity(body, contentType);
			Invocation invocation = builder.buildPost(entity);
			response = invocation.invoke();
		}
		
		return response;
	}
	
	/**
	 * Perform a PUT request. Query parameters are provided as a Map, or may
	 * be null. body may be null.
	 * IMPORTANT: It is essential to call close() on the Response object.
	 */
	protected Response makePutRequest(String path, MediaType contentType,
		String body, String[]... params) throws Exception {
		
		Invocation.Builder builder = createHttpRequestBuilder(path, params);
		Response response = null;
		if (body == null) {
			response = builder.buildPut(null).invoke();
		} else {
			Entity<String> entity = Entity.entity(body, contentType);
			Invocation invocation = builder.buildPost(entity);
			response = invocation.invoke();
		}
		
		return response;
	}
	
	protected Invocation.Builder createHttpRequestBuilder(String path, String[]... params)
		throws Exception {
		
		URI uri = new URI(path);
		
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
			.register("http", new PlainConnectionSocketFactory())
			.build();
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.connectorProvider(new ApacheConnectorProvider());
		clientConfig.property(CommonProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);

		BasicHttpClientConnectionManager connManager =
			new BasicHttpClientConnectionManager(socketFactoryRegistry);
		//clientConfig.register(ResponseStatusExceptionFilter.class);
		//clientConfig.register(JsonClientFilter.class);
		//clientConfig.register(JacksonJsonProvider.class);
		clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, connManager);
	
		ClientBuilder clientBuilder = ClientBuilder.newBuilder().withConfig(clientConfig);
		
		Client client = clientBuilder.build();
		
		WebTarget target = client.target(path);
		
		for (String[] keyValuePair : params) {
			if (keyValuePair.length != 2) throw new RuntimeException(
				"Expected a key, value pair; found: " + keyValuePair.toString());
			target = target.queryParam(keyValuePair[0], keyValuePair[1]);
		}
		
		Invocation.Builder invocationBuilder =
			target.request(MediaType.TEXT_PLAIN_TYPE);
		
		return invocationBuilder;
	}
