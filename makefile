# Written by Cliff Berg.

# This makefile contains no information about file structure or tool locations.
# All such configurations should be made in makefile.inc

include makefile.inc

.ONESHELL:
.SUFFIXES:
export SHELL = /bin/bash

# Artifact names:
export ORG = Cliff Berg
export GroupId = com.cliffberg
export MavenBaseArtifactId = livesticker
export PRODUCT_NAME = livesticker
export Description = "Live Sticker"

# Output artifact names:
export package := com/cliffberg/livesticker
export test_package=com/cliffberg/livesticker/test
export package_name = com.cliffberg.livesticker
export test_package_name = com.cliffberg.livesticker.test

# Relative locations:
export ThisDir := $(shell pwd)
export test_src_dir := $(ThisDir)/test
export test_package = $(package)/test
export javadoc_dir := $(ThisDir)/docs
export test_jar := $(MVN_REPO)/com/cliffberg/livesticker/test/$(VERSION)/test-$(VERSION).jar
export JAVA_MEDIA_JAR := $(HOME)/.m2/repository/javax/media/jmf/2.1.1e/jmf-2.1.1e.jar
export SOUNDRECORDER_JAR := $(HOME)/.m2/repository/com/cliffberg/livesticker/soundrecorder/0.1/soundrecorder-0.1.jar
export TRANSCRIBER_JAR := $(HOME)/.m2/repository/com/cliffberg/livesticker/transcriber/0.1/transcriber-0.1.jar

# Aliases:
test := java -cp $(CUCUMBER_CLASSPATH):$(test_jar):$(TRANSCRIBER_JAR):$(ThirdPartyJarDir)/*
test := $(test) cucumber.api.cli.Main --glue com.cliffberg.livesticker.test
test := $(test) $(test_src_dir)/features


################################################################################
# Tasks


.PHONY: all mvnversion gen_config jars javadoc record test cukehelp cukever clean info soundrecorder transcriber tests

all: gen_config jars image

mvnversion:
	$(MVN) --version


# ------------------------------------------------------------------------------
# Create the directory that will contain the parser source files.
$(sable_dabl_out_dir):
	mkdir -p $(sable_dabl_out_dir)/$(package)

# ------------------------------------------------------------------------------
# Create the directories that will contain the compiled class files.
$(build_dir):
	mkdir -p $(build_dir)

# ------------------------------------------------------------------------------
# Create the directory that will contain the jar files that are created.
$(jar_dir):
	mkdir -p $(jar_dir)

# ------------------------------------------------------------------------------
# Create directory for third party jars - needed to assemble jars for image.
$(ThirdPartyJarDir):
	mkdir -p $(ThirdPartyJarDir)


# ------------------------------------------------------------------------------
# Generate the Config class that the runtime uses to determine the version of DABL.
gen_config:
	echo "package com.cliffberg.livesticker;" > $(common_src_dir)/$(package)/Config.java
	echo "public class Config {" >> $(common_src_dir)/$(package)/Config.java
	echo "public static final String Version = \"$(VERSION)\";" >> $(common_src_dir)/$(package)/Config.java
	echo "}" >> $(common_src_dir)/$(package)/Config.java

# ------------------------------------------------------------------------------
# Build all four components.
jars:
	$(MVN) clean install

# ------------------------------------------------------------------------------
# For testing:

soundrecorder:
	$(MVN) clean install --projects soundrecorder

transcriber:
	$(MVN) clean install  -projects transcriber

tests:
	$(MVN) clean install  -projects test

# ------------------------------------------------------------------------------
# Generate javadocs for all modules.
javadoc:
	$(MVN) javadoc:javadoc


# ------------------------------------------------------------------------------
# Deploy for testing.

# For debugging: list dependencies.
showdeps:
	$(MVN) dependency:build-classpath --projects client | tail -n $(mvn_spaces) | head -n 1 | tr ":" "\n" > client_jars.txt
	$(MVN) dependency:build-classpath --projects common | tail -n $(mvn_spaces) | head -n 1 | tr ":" "\n" > common_jars.txt
	sort -u client_jars.txt common_jars.txt

# Identify the jar files that need to be added to the image.
copydeps: $(ThirdPartyJarDir)
	$(MVN) dependency:build-classpath --projects transcriber | tail -n $(mvn_spaces) | head -n 1 | tr ":" "\n" > transcriber_jars.txt
	$(MVN) dependency:build-classpath --projects test | tail -n $(mvn_spaces) | head -n 1 | tr ":" "\n" > test_jars.txt
	{ \
	cp=`sort -u transcriber_jars.txt test_jars.txt` ; \
	for path in $$cp; do cp $$path $$ThirdPartyJarDir; done; \
	}	

# ------------------------------------------------------------------------------
# Execute the Sound Recorder to create the WAV file needed by the transcription tests.
# This requires the user to say "Once upon a midnight dreary" in less than 2000ms.
record:
	$(JAVA) -cp $(SOUNDRECORDER_JAR):$(JAVA_MEDIA_JAR) com.cliffberg.livesticker.SoundRecorder 2000

# ------------------------------------------------------------------------------
# Run Cucumber tests.
# Note: We could export LD_LIBRARY_PATH instead of passing it in the java command.
awstest:
	./run_with_context.sh $(AWSCredentials) "$(test) --tags @aws"

mstest:
	./run_with_context.sh $(MicrosoftCredentials) "$(test) --tags @microsoft"

dryrun:
	./run_with_context.sh $(AWSCredentials) "$(test) --dry-run --tags @aws"

cukehelp:
	java -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --help

cukever:
	java -cp $(CUCUMBER_CLASSPATH) cucumber.api.cli.Main --version

clean: compile_clean test_clean clean_parser clean_task_parser
	rm -f Manifest
	rm -r -f $(javadoc_dir)/*

info:
	@echo "Makefile for $(PRODUCT_NAME)"
