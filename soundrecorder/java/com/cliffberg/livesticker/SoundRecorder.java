package com.cliffberg.livesticker;

import javax.sound.sampled.*;
import java.io.*;

/**
 * Ref: http://www.codejava.net/coding/capture-and-record-sound-into-wav-file-with-java-sound-api
 */
public class SoundRecorder {
	// path of the wav file
	File wavFile = new File("RecordAudio.wav");

	// format of audio file
	AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;

	// the line from which audio data is captured
	TargetDataLine line;

	/**
	 * Defines an audio format
	 * Ref: https://docs.oracle.com/javase/7/docs/api/javax/sound/sampled/AudioFormat.html
	 * Ref: https://docs.microsoft.com/en-us/azure/cognitive-services/speech/getstarted/getstartedrest?tabs=Powershell
	 */
	AudioFormat getAudioFormat() {
		
		return new AudioFormat(
			16000.0,	// sampleRate
			8,			// sampleSizeInBits
			1,			// channels - mono is required by Microsoft Speech system.
			true,		// signed
			true		// bigEndian
			);
	}

	/**
	 * Captures the sound and record into a WAV file
	 */
	public void start() {
		try {
			AudioFormat format = getAudioFormat();
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);

			// checks if system supports the data line
			if (!AudioSystem.isLineSupported(info)) {
				System.out.println("Line not supported");
				System.exit(0);
			}
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(format);
			line.start();	// start capturing

			System.out.println("Start capturing...");

			AudioInputStream ais = new AudioInputStream(line);

			System.out.println("Start recording...");

			// start recording
			AudioSystem.write(ais, fileType, wavFile);

		} catch (LineUnavailableException ex) {
			ex.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Closes the target data line to finish capturing and recording
	 */
	void finish() {
		line.stop();
		line.close();
		System.out.println("Finished");
	}

	/**
	 * Entry to run the program
	 */
	public static void main(String[] args) {
		
		long recordTime = 0;
		
		try {
			recordTime = Long.parseLong(args[0]);
		} catch (Exception ex) {
			System.out.println("requires one argument: the recording time in ms");
			System.exit(1);
		}
		
		final SoundRecorder recorder = new SoundRecorder();
		final long RecordTime = recordTime;

		// creates a new thread that waits for a specified
		// of time before stopping
		Thread stopper = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(RecordTime);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				recorder.finish();
			}
		});

		stopper.start();

		// start recording
		recorder.start();
	}
}