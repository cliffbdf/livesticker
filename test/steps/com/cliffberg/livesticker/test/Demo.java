package com.cliffberg.livesticker.test;

import cucumber.api.Format;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.Reader;
import java.io.StringReader;
import java.io.File;

import java.net.URI;

/**
 * Note: before running this test, (1) a wav file named RecordAudio.wav must be created,
 * and (2) the environment variables AWSBucketName and AWSSecretKey must be set.
 */
public class Demo extends TestBase {
	
	private String audioFilePath = "RecordAudio.wav";
	private String bucketName = System.getenv("AWSBucketName");
	private String aws_access_key_id = System.getenv("aws_access_key_id");
	private String aws_secret_access_key = System.getenv("aws_secret_access_key");
	private String ms_subscriptionKey = System.getenv("MSCognitiveServicesKey1");

	private File file;
	private String transcription;
	
	@Before("@smoke")
	public void beforeEachScenario() throws Exception {
	}
	
	@After("@smoke")
	public void afterEachScenario() throws Exception {
	}
	

	/* -----------------------------------------------------------------------
	 * Scenario: Perform a transcription in AWS
	 */
	
	@Given("^I have a message recording and the bucket name and key have been set$")
	public void i_have_a_message_recording_and_bucket_and_key_set() throws Exception {
		this.file = new File(audioFilePath);
		assertThat(this.file.exists(), "Audio file " + this.file.getAbsolutePath() + " not found");
		
		this.bucketName = System.getenv("AWSBucketName");
		assertThat(this.bucketName != null, "AWSBucketName not set");
		
		this.aws_access_key_id = System.getenv("aws_access_key_id");
		assertThat(this.aws_access_key_id != null, "aws_access_key_id not set");
		
		this.aws_secret_access_key = System.getenv("aws_secret_access_key");
		assertThat(this.aws_secret_access_key != null, "aws_secret_access_key not set");
	}
	
	@When("^I push the file to S3 and transcribe the recording$")
	public void i_push_to_S3_and_transcribe_the_recording() throws Exception {
		com.cliffberg.livesticker.aws.Transcriber transcriber = 
			new com.cliffberg.livesticker.aws.Transcriber();
		URI uri = transcriber.pushFileToS3(this.file, this.bucketName, 
			this.aws_access_key_id, this.aws_secret_access_key);
		this.transcription = transcriber.transcribe(uri, this.aws_access_key_id,
			this.aws_secret_access_key);
	}
	
	@Then("^the saved recording gets transcribed to \"([^\"]*)\"$")
	public void the_saved_recording_gets_transcribed(String expectedText) throws Exception {
		assertThat(this.transcription.equals(expectedText),
			"Text mismatch: expected " + expectedText + "; obtained " + this.transcription);
	}

	/* -----------------------------------------------------------------------
	 * Scenario: Perform a transcription in Azure
	 */
	
	@Given("^I have a message recording and the key has been set$")
	public void i_have_a_message_recording_and_key_set() throws Exception {
		this.file = new File(audioFilePath);
		assertThat(this.file.exists(), "Audio file " + this.file.getAbsolutePath() + " not found");
		
		this.ms_subscriptionKey = System.getenv("MSCognitiveServicesKey1");
		assertThat(this.ms_subscriptionKey != null, "MSCognitiveServicesKey1 not set");
	}
	
	@When("^I transcribe the recording$")
	public void i_transcribe_the_recording() throws Exception {
		com.cliffberg.livesticker.azure.Transcriber transcriber = 
			new com.cliffberg.livesticker.azure.Transcriber();
		this.transcription = transcriber.transcribe(this.file, this.ms_subscriptionKey);
	}
}
