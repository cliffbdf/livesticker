package com.cliffberg.livesticker.test;

import java.util.List;
import java.util.stream.Stream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Utilities shared by the DABL Cucumber test suite.
 * Ref for JVM hooks: http://zsoltfabok.com/blog/2012/09/cucumber-jvm-hooks/
 */
public class TestBase {

	public TestBase() {
	}
	
	public void msg(String message) {
		System.out.println(message);
	}
	
	/**
	 * If expr is false, print the message and throw an Exception.
	 */
	public static void assertThat(boolean expr, String msg) {
		if (msg == null) msg = "";
		if (msg != null) msg = "; " + msg;
		if (! expr) throw new RuntimeException("Assertion violation: " + msg);
	}
	
	/**
	 * If the node is not class c, throw an exception.
	 */
	public static void assertIsA(Object n, Class c) {
		if (! c.isAssignableFrom(n.getClass())) throw new RuntimeException("Assertion violation");
	}
	
	/**
	 * If expr is false, perform the specified action and then throw an Exception.
	 */
	public static void assertThat(boolean expr, Runnable action) {
		if (! expr) {
			System.out.println("Assertion violation; performing diagnostic action...");
			try {
				action.run();
			} finally {
				System.out.println();
				System.out.println("End of diagnostic action.");
			}
			throw new RuntimeException("Assertion violation");
		}
	}
}
