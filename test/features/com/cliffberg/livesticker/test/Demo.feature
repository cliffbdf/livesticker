# language: en

@task
Feature: Demo
	
	@aws
	Scenario: Perform a transcription in AWS
		Given I have a message recording and the bucket name and key have been set
		When I push the file to S3 and transcribe the recording
		Then the saved recording gets transcribed to "Once upon a midnight, dreary."
	
	@microsoft
	Scenario: Perform a transcription in Azure
		Given I have a message recording and the key has been set
		When I transcribe the recording
		Then the saved recording gets transcribed to "Once Upon a midnight dreary."
	
